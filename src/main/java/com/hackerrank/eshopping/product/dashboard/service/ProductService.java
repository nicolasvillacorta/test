package com.hackerrank.eshopping.product.dashboard.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackerrank.eshopping.product.dashboard.model.Product;
import com.hackerrank.eshopping.product.dashboard.model.dto.UpdateDto;
import com.hackerrank.eshopping.product.dashboard.repositories.ProductDao;

@Service
public class ProductService {
	
	@PersistenceContext
	EntityManager entityManager; 
	
	@Autowired
	ProductDao productDao;
	
	public Product createProduct(Product product) {
		
		Product prod = productDao.findById(product.getId()).orElse(null);
		
		if(prod==null)
			return productDao.save(product);
		else
			return null;
	}
	
	public Product updateProduct(UpdateDto dto, long id) {
		
		Product prodFound = productDao.findById(id).orElse(null);
		System.out.println(prodFound);

		if(prodFound != null) {
			prodFound.setAvailability(dto.getAvailability());
			prodFound.setRetailPrice(dto.getRetail_price());
			prodFound.setDiscountedPrice(dto.getDiscounted_price());
			productDao.save(prodFound);
			
			return prodFound;
		} else 
			return null;
		
	}
	
	public Product findProduct(long id) {
		
		Product prod = productDao.findById(id).orElse(null);
		
		return prod;
	}
	
	public List<Product> findByCategory(String category){
		return productDao.findByCategoryOrderByAvailabilityDescDiscountedPriceAscIdAsc(category);
	}
	
	@SuppressWarnings("unchecked")
	public List<Product> findByCategoryAndAvailability(String category, boolean availability){

		Query query = entityManager.createNativeQuery(
				"SELECT * FROM products WHERE category = :category AND availability = :availability "
				+ "ORDER BY (retail_price - discounted_price)/100 DESC, discounted_price ASC, id ASC", Product.class);
		query.setParameter("category", category);
		query.setParameter("availability", availability);
		
		List<Product> sortedProducts = query.getResultList();
		
		return sortedProducts;
	}
	
	public List<Product> findAll(){
		return productDao.findAll();
	}

}
