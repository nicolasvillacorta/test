package com.hackerrank.eshopping.product.dashboard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hackerrank.eshopping.product.dashboard.model.Product;
import com.hackerrank.eshopping.product.dashboard.model.dto.UpdateDto;
import com.hackerrank.eshopping.product.dashboard.service.ProductService;

@RestController
@RequestMapping(value = "/products")
public class ProductsController {
	
	@Autowired
	ProductService productService;
	
	// Funcionality 1: Add a product.
	@PostMapping
	public ResponseEntity<Product> createProduct(@RequestBody Product product){
		Product foundProd = productService.createProduct(product);
		
		if(foundProd==null)
			return ResponseEntity.status(400).build();
		else
			return ResponseEntity.ok(foundProd);
	}
	
	// Funcionality 2: Update a product by id
	@PutMapping(path="/{product_id}")
	public ResponseEntity<Product> updateProduct(@RequestBody UpdateDto updateDto,
			@PathVariable("product_id") long id){
		
		Product updatedProd = productService.updateProduct(updateDto, id);
		
		if(updatedProd!=null)
			return ResponseEntity.ok(updatedProd);
		else
			return ResponseEntity.status(404).build();
	}
	
	// Funcionality 3: Return a product by id.
	@GetMapping(path="/{product_id}")
	public ResponseEntity<Product> findProduct(@PathVariable("product_id") long id){

		Product foundProd = productService.findProduct(id);
		
		if(foundProd==null)
			return ResponseEntity.status(400).build();
		else
			return ResponseEntity.ok(foundProd);
		
	}
	
	// Funcionality 4: Return products by category
	@GetMapping(params="category")
	public ResponseEntity<List<Product>> findByCategory(@RequestParam("category") String category){
		
		List<Product> products = productService.findByCategory(category);
		
		if(products.isEmpty())
			return ResponseEntity.status(404).build();
		else
			return ResponseEntity.ok(products);
		
	}
	
	// Funcionality 5: Return products by category and availability.
	@GetMapping(params= {"category", "availability"})
	public ResponseEntity<List<Product>> findByCategoryAndAvailability(
			@RequestParam("category") String category,
			@RequestParam("availability") boolean availability){
		
		List<Product> products = productService.findByCategoryAndAvailability(category, availability);
		
		if(products.isEmpty())
			return ResponseEntity.status(404).build();
		else
			return ResponseEntity.ok(products);
		
	}
	

//	 Funcionality 6: Return all products.
	@GetMapping
	public ResponseEntity<List<Product>> findAllProducts(){
		
		List<Product> products = productService.findAll();
		
		return ResponseEntity.ok(products);
	}
}
